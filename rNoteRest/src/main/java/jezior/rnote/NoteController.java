package jezior.rnote;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NoteController 
{
	
	@Autowired
	private NoteRepository repository;

    @RequestMapping(value="/note", method=RequestMethod.GET)
    public List<NoteDto> get_notes(@RequestParam(value="userId", defaultValue="World") String userId) 
    {
        List<NoteDto> list = new ArrayList<>();
        repository.findByUserId(userId).forEach(list::add);
        return list;
    }
    
    @RequestMapping(value="/note", method=RequestMethod.PUT)
    public NoteDto add_note(@RequestBody NoteDto note)
    {
    	return repository.save(note);
    }
    
    @RequestMapping(value="/note", method=RequestMethod.POST)
    public NoteDto update_note(@RequestBody NoteDto note)
    {
    	return repository.save(note);
    }
    
    @RequestMapping(value="/note", method=RequestMethod.DELETE)
    public int delete_note(@RequestBody NoteDto note)
    {
    	repository.delete(note);
    	return 0;  	
    }
}
