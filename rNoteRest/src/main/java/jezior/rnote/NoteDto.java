package jezior.rnote;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity(name="Note")
public class NoteDto 
{
	
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="note_id_seq")
    @SequenceGenerator(name="note_id_seq", sequenceName="note_id_seq", allocationSize=1)

	private long idNote;
    
	private String userId;
	private Date createdOn;
	private Date updatedOn;
	private String noteText;
	private int flags;
	
	protected NoteDto()
	{	
	}
	
	public NoteDto(String userId, Date createdOn, Date updatedOn, String noteText, int flags)
	{
		this.setUserId(userId);
		this.setCreatedOn(createdOn);
		this.setUpdatedOn(updatedOn);
		this.setNoteText(noteText);
		this.setFlags(flags);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getNoteText() {
		return noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public int getFlags() {
		return flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}

	public long getIdNote() {
		return idNote;
	}

	public void setIdNote(long idNote) {
		this.idNote = idNote;
	}
}
