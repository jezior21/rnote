package jezior.rnote;

public class Greeting {

    private final long id;
    private final String content;
    private final String truth;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
        this.truth = "Some testing";
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
    
    public String getTruth() {
        return truth;
    }
}