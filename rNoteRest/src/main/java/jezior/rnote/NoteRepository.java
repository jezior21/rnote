package jezior.rnote;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface NoteRepository extends CrudRepository<NoteDto,Long>{
	List<NoteDto> findByUserId(String userId);
}
