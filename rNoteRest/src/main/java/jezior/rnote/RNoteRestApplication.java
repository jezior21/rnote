package jezior.rnote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RNoteRestApplication {

	public static void main(String[] args) {
		System.out.println("Tests");
		SpringApplication.run(RNoteRestApplication.class, args);
	}
}
