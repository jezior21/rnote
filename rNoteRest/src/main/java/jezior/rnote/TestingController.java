package jezior.rnote;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestingController {
	
	@Autowired
	private NoteRepository repository;

    @RequestMapping(value="/test", method=RequestMethod.POST)
    public String test() {
    	try
    	{
            repository.count();
            return "OK";
    	}
    	catch(Exception ex)
    	{
    		StringWriter sw = new StringWriter();
    		PrintWriter pw = new PrintWriter(sw);
    		ex.printStackTrace(pw);
    		return sw.toString();
    	}
    }
    
    @RequestMapping(value="/test", method=RequestMethod.GET)
    public NoteDto get()
    {
    	return new NoteDto("123", new Date(), new Date(), "Testing note", 0);
    }
}